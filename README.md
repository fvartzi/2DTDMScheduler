# TDM Test Scheduling and TAM Optimization Tool - 2DTDMSTO
A new toolkit for testing DVFS-based SoCs with multiple voltage islands using TDM.

**Requirements:** 
1. Windows (>10) OS (Toolkit can be compiled for other OS too), 
2. SQL Server Express 2019 SQLLocalDB (>v13) (Optional).
3. Windows Subsystem for Linux (WSL) and a Linux variant for WSL (Optional, to support thermal aware testing results using teh HOTSPOT tool).
4. .Net requirements as automatically are requested by OS.

**Installation:**
1. Download the installation files from 2DTDMSTO folder in this project.
2. (Optional) Install SQL Server Express 2019 SQLLocalDB (>v13). SQLLocalDB is required to connect to 2DTDMS toolkit using credentials. This will allow user to activate email notifications and encrypt test processes.
3. Double-click "setup.exe" file and follow the toolkit wizard. User can select the installation folder. 

**First Steps:**
1. Locate the "2DTDMScheduler.exe" file in the installation folder and optionally create a shortcut to desktop.
2. Double-click "2DTDMScheduler" to activate toolkit's Login Window.
3. In Login Window, enter as username **"test@tdm.sch"**, as pwd **"testtdm"** and push the login button, in case that SQLLocalDB has been installed. If not, user can choose credentials of choice and just push the login button.
4. Go to Configuration Tab, change your credentials accordingly (it is not required) and select as 2D TDM Console App the **"2DTDMConsole.exe"** file from installation folder (it is required the full path along with the file's name).
5. 2DTDMSTO toolkit can be combined with **HOTSPOT** tool. Optionally, Install HOTSPOT in a Linux variant for WSL (preferably Ubuntu 24.04) and add HOTSPOT's installation folder to PATH environment variable of the linux terminal.
6. Go to Configuration Tab and select HOTSPOT's config file. HOTSPOT's config file can be in a directory of user's choice with write access. There is an example config file in the installation folder. 
(User can add old "hotspot.exe" and "hotfloorplan.exe" files along with the hotspot config file, in a directory with write access, and then change the "WSL" switch in the Configuration tab to "WIN". However, it is not recommended.)

**Using the 2DTDMSTO toolkit:**

To showcase the capabilities of the 2DTDMS toolkit, two artificial DVFS-based SoCs are utilized: one with 13 cores, 3 voltage islands, and 3 voltage levels, and a larger one with 57 cores, 9 voltage islands, and 3 voltage levels. Each SoC includes benchmark cores, defined by the Core Definition Files (.cdf) located in the corresponding directory (\2DTDMScheduler\2DTDMSTO\0 Create-Get Core Definition Files).

1. In the **2DTDMSTO\1 Create Test Process** directory, the **".init2D"** files enable the automatic creation of a Test Process for the corresponding artificial SoC. Each file contains a description of the selected artificial DVFS-based SoC and a detailed specification of a TDM-based test process. Notably, the **".cdf"** files required by these artificial SoCs are already embedded in the toolkit. Regarding the Test Process, the toolkit analyzes the requirements and generates a TAM that balances the test loads of the cores based on the specified number of buses.

    Now, to create a test process for the selected SoC, the user should select the **"Use Benchmark Cores"** option from the **"C"** (Create) menu button. A window will then appear, prompting the user to name the SoC's test process.

    Next, a message will appear requesting the selection of an initialization file (".init2D"). The user should click **OK** and then select one of the two initialization files. The necessary files for the test process will be generated in a new folder named according to the user's selection in the previous step. **Users can** easily **produce** their **initialization files**, using the existed ones as templates.

    If **Hotspot** is available, the user can also generate a floorplan for the artificial SoC, which can be used for thermal analysis. In this case, the **floorplan switch** in the configuration tab must be activated before proceeding with any steps.

2. To execute a test process and derive a test schedule for the selected SoC, user should select initially the option **"Load DUT"** from menu button **"R"** (RUN). Then, user should select the ".soc" file in the test processes's folder. The toolkit will analyse and verify the selected file and it will prompt user to go to **Configuration Tab**. Now, user can activate new options or change parameters related to the test process (e.g Power constraints). 

3. Next, the user should select **"Load TAM"** from the **"R"** (RUN) menu and load the ".tam" file. The toolkit will analyse and verify the file and prompt the user to return to the **Configuration Tab** for a final review. User can activate new options or change parameters related to the test process and computational methods.

4.  Finally, to generate the test schedule, the user should select **"Derive Schedule"** from the **"R"** (RUN) menu. This process creates an ".r2d" file in the test process folder. The user can **visualize** the test schedule using the **"Test Schedule Graph"** option in the **"S"** (Schedule) menu.

5.  The user can **visualize** a power-annotated test schedule using the **Power-annotated Schedule Graph"** option in the **"S"** (Schedule) menu. Right click on schedule will provide with power-annotated data at the selected test time.

6.  The user can **visualize** a thermally-annotated test schedule using the **Thermal-annotated Schedule Graph"** option in the **"S"** (Schedule) menu.  Right click on schedule will provide with thermally-annotated data at the selected test time.

7.  The user can **visualize** a power, tharmal and floorplan-based graphs using correspondingly the options in **"P"** (Power), **"T"** (Thermal) and **"F"** (Floorplan) menu.

8. To execute TAM optimization and derive a TAM configuration for the selected SoC, user should select initially the option **"Load DUT"** from menu button **"O"** (Optimize). Then, user should select the ".soc" file in the test processes's folder. The toolkit will analyse and verify the selected file and it will prompt user to go to **Configuration and TAM Tab**. Now, user can specify the TAM optimization process (e.g Solver,  ATE parameters, select normal or procedure for large SoC).

9.  To generate the optimized TAM configuration, the user should select **"TAM Design Action"** from the **"O"** (RUN) menu. This process creates an ".t2d" file in the test process folder.

10.  Finally, to generate the optimized test process, the user should select **"DCreate Testbench"** from the **"O"** (Optimize) menu. This process creates a ".soc" and a ".tam" file in the test process folder. The user can go to step **2**.

This program is distributed as proof-of-concept for SoC TDM Test Scheduling and TAM optimization, in the hope that it will be useful,
but WITHOUT ANY WARRANTY;
